import 'dart:convert';
import 'dart:io';

import 'package:quoter/models/QuoteResponse.dart';

class BackendProvider {

  static const String BASE_URL = 'https://quotes.rest/';

  Future<QuoteResponse> getResponseData() async {
    var apiUrl = Uri.parse(BASE_URL + 'qod?language=en');
    var client = HttpClient(); //

    HttpClientRequest request = await client.getUrl(apiUrl);
    HttpClientResponse response = await request.close();

    var resStream = response.transform(Utf8Decoder());
    String json = "";
    await for (var data in resStream) {
      json += data;
    }

    json = "{ \"success\":{ \"total\":1 }, \"contents\":{ \"quotes\":[ { \"quote\":\"Many of life's failures are experienced by people who did not realize how close they were to success when they gave up.\", \"length\":\"119\", \"author\":\"ThomasEdison\", \"tags\":[ \"inspire\", \"success\" ], \"category\":\"inspire\", \"language\":\"en\", \"date\":\"2020-02-20\", \"permalink\":\"https://theysaidso.com/quote/thomas-edison-many-of-lifes-failures-are-experienced-by-people-who-did-not-reali\", \"id\":\"qnI5_EBubjhw9eFkE2ZsrweF\", \"background\":\"https://theysaidso.com/img/qod/qod-inspire.jpg\", \"title\":\"InspiringQuoteoftheday\" } ] }, \"baseurl\":\"https://theysaidso.com\", \"copyright\":{ \"year\":2022, \"url\":\"https://theysaidso.com\" } } "
        "";

    return QuoteResponse.fromJson(jsonDecode(json));
  }

}