import 'package:flutter/widgets.dart';
import 'package:quoter/home/HomeModel.dart';
import 'package:quoter/home/HomeView.dart';
import 'package:quoter/mvc_core/Controller.dart';
import 'package:quoter/mvc_core/Event.dart';

class HomeController extends Controller {

  HomeController() {
    create(HomeView(), HomeModel());
  }

  @override
  State<StatefulWidget> createState() => HomeControllerState();

  @override
  void eventHandler(Event event) {
    event.valueListener((model as HomeModel).getQuote());
  }
}

class HomeControllerState extends ControllerState {
  @override
  Widget build(BuildContext context) {
    return widget.view.viewWidget;
  }

}