import 'package:flutter/material.dart';
import 'package:quoter/models/Quote.dart';
import 'package:quoter/mvc_core/Event.dart';
import 'package:quoter/mvc_core/View.dart';

class HomeView extends StatefullView {
  Widget getQuoteButton;
  Widget content = Text("");

  @override
  State<StatefulWidget> createState() => HomeViewState();
}

class HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    widget.getQuoteButton = RaisedButton(
        child: Text("Get quote"),
        onPressed: () {
          widget.notifyController(Event(widget.getQuoteButton, onQuoteReady));
        });

    return Scaffold(
        appBar: AppBar(
          title: Text("Quote of the day"),
        ),
        body: Center(
            child: Column(
          children: [widget.content, widget.getQuoteButton],
        )));
  }

  void onQuoteReady(Future<dynamic> quote) {
    setState(() {
      widget.content = FutureBuilder(
          future: quote,
          builder: (context, quote) {
            Quote q = quote.data as Quote;
            if (q == null) return Text("Loading... please wait!!");
            if (quote.error != null) {
              return Text("Error occured");
            } else {
              return Column(children: [Text(q.quote), Text(q.author)]);
            }
          });
    });
  }
}
