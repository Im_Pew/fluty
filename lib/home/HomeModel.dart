import 'dart:async';

import 'package:quoter/backendprovider/BackendProvider.dart';
import 'package:quoter/mvc_core/Model.dart';

class HomeModel extends Model {
  Future<dynamic> getQuote() async {
    return BackendProvider().getResponseData().asStream().transform(StreamTransformer.fromHandlers(
      handleData: (streamData, sink) {
        sink.add(streamData.quotes.first);
      }
    )).single;
  }
}
