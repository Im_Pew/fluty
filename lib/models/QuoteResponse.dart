import 'package:quoter/models/Quote.dart';

class QuoteResponse {
  List<Quote> quotes;

  QuoteResponse({this.quotes});

  factory QuoteResponse.fromJson(Map<String, dynamic> json) {
    List<Quote> parsedData = new List<Quote>();

    print("Object data: " + json.toString());
    json.forEach((key, value) {
      if (key == "contents") {
        (value as Map<String, dynamic>).forEach((key, value) {
          if (key == "quotes") {
            (value as List<dynamic>).forEach((element) {
              parsedData.add(Quote.fromJson(element));
            });
          }
        });
      }
    });

    return new QuoteResponse(
      quotes: parsedData,
    );
  }
}
