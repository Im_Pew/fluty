class Quote {
  String id;
  String quote;
  int length;
  String author;
  String category;
  String background;

  Quote(
      {this.quote,
      this.id,
      this.length,
      this.author,
      this.category,
      this.background});

  factory Quote.fromJson(Map<String, dynamic> json) {
    return new Quote(
        id: json['id'],
        quote: json['quote'],
        length: int.tryParse(json['length']),
        author: json['author'],
        category: json['category'],
        background: json['background']);
  }
}
