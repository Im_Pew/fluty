import 'package:flutter/widgets.dart';

class Event {

  Event(this.sender, this.valueListener);

  Widget sender;
  dynamic valueListener;

  Widget getSender() {
    return sender;
  }

  dynamic getListener() {
    return valueListener;
  }
}