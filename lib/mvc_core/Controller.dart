import 'dart:async';

import 'package:flutter/material.dart';
import 'package:quoter/mvc_core/Event.dart';
import 'package:quoter/mvc_core/View.dart';

import 'Model.dart';

abstract class Controller extends StatefulWidget {
  StreamController<Event> streamController;
  View view;
  Model model;
  BuildContext context;

  void createWithoutModel(View view) {
    create(view, null);
  }

  void create(View view, Model model) {
    streamController = StreamController();
    this.model = model;
    this.view = view;
    view.addNotifier(streamController);

    streamController.stream.listen(eventHandler);
  }

  void eventHandler(Event event);
}

abstract class ControllerState<ExtendedController extends Controller>
    extends State<ExtendedController> {
  @override
  void initState() {
    super.initState();

    widget.context = context;
  }

  @override
  void didUpdateWidget(ExtendedController oldWidget) {
    super.didUpdateWidget(oldWidget);

    widget.context = oldWidget.context;
  }

  @override
  void dispose() {
    widget.streamController.close();
    widget.context = null;

    super.dispose();
  }
}
