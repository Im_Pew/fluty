import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:quoter/mvc_core/Event.dart';

abstract class StatefullView extends StatefulWidget with View {
  StatefullView() {
    this.viewWidget = this;
  }
}

abstract class StatelessView extends StatelessWidget with View {
  StatelessView() {
    this.viewWidget = this;
  }
}

abstract class View {
  Widget viewWidget;
  StreamController controllerNotifier;

  void addNotifier(StreamController notifier) {
    this.controllerNotifier = notifier;
  }

  void notifyController(Event event) {
    controllerNotifier.add(event);
  }
}
