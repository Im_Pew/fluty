import 'package:flutter/material.dart';
import 'package:quoter/home/HomeController.dart';
import 'package:quoter/main.dart';

class Router {
  static const String ROUTE_HOME = "home";
  static const String ROUTE_MAIN = "main";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case ROUTE_MAIN:
        return new MaterialPageRoute(
            builder: (_) => MyHomePage(title: "Title"));
      case ROUTE_HOME:
        return new MaterialPageRoute(builder: (_) => HomeController());
    }
    return new MaterialPageRoute(
        builder: (_) =>
            MyHomePage(title: "Error")); // or Page with error message
  }
}
